const mutation = require('../utils/mutation');
const mongoDB = require('../store/mongo')
const DNASModel = require('../store/schema/dna')
class DNAService {
  constructor() {
  }

  async hasMutatio({dna}) {
    const isMutation = mutation(dna);
    const newDNA = { dna, isMutation };
    await mongoDB.create(DNASModel,newDNA)
    return isMutation || false;
  }
}


module.exports = DNAService;