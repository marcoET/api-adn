require('dotenv').config();

const config = {
  api: {
    port: process.env.PORT || 3000,
    cors: process.env.CORS || '*',
  },
  dev: process.env.NODE_ENV !== 'production',
  sentry: {
    sentryDns: process.env.SENTRY_DNS,
    sentryId: process.env.SENTRY_ID,
  },
  database:{
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD,
    dbHost: process.env.DB_HOST,
    dbName: process.env.DB_NAME
  }
};

module.exports = config;
