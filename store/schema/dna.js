const mongoose = require('mongoose');
const { Schema } = mongoose;

const dnaSchema = new Schema({
  dna: { type: [String], required: true, unique: true },
  isMutation: { type: Boolean, required: true },
});

const modelDNA = mongoose.model('dna', dnaSchema);
module.exports = modelDNA;
