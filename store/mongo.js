const mongoose = require('mongoose');
const { database } = require('../config');

const USER = encodeURIComponent(database.dbUser);
const PASSWORD = encodeURIComponent(database.dbPassword);
const MONGO_URI = `mongodb+srv://${USER}:${PASSWORD}@${database.dbHost}/${database.dbName}?retryWrites=true&w=majority`;
console.log(MONGO_URI);
mongoose.Promise = global.Promise;
mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
console.info('[DB] Connection successfully');

function create(Model, data) {
  const newRecord = new Model(data);
  return newRecord.save();
}

module.exports = { create };
