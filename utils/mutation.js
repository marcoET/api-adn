const baseRegExp =
  '[%BASE%]{1}[\\w]{%NUM%}[%BASE%]{1}[\\w]{%NUM%}[%BASE%]{1}[\\w]{%NUM%}[%BASE%]{1}';
const bases = ['A', 'T', 'C', 'G'];

function hasMutation(dnaArray) {
  const dna = dnaArray.join("");

  if (!validDNA(dna)) {
    return false;
  }

  if (findMutationHorizontal(dna)) {
    return false;
  }

  if (findMutationVertical(dna)) {
    return false;
  }

  if (findMutationDiagonal(dna)) {
    return false;
  }


  return true;
}

function validDNA(dna) {
  const regexpDNA = new RegExp('[ATCG]{36}', 'g');
  return testDNA(regexpDNA, dna);
}

function findMutationHorizontal(dna) {
  const regexpMutationHorizontal = new RegExp('[A]{4}|[T]{4}|[C]{4}|[G]{4}');
  return testDNA(regexpMutationHorizontal, dna);
}

function findMutationVertical(dna) {
  for (const base of bases) {
    let newregexp = baseRegExp.replace(/%BASE%/gi, base);
    newregexp = newregexp.replace(/%NUM%/gi, 5);
    const regexpMutationVertical = new RegExp(newregexp, 'g');
    const isMutation = testDNA(regexpMutationVertical, dna);
    if (isMutation) {
      return true;
    }
  }

  return false;
}

function findMutationDiagonal(dna) {
  for (const base of bases) {
    let newregexp = baseRegExp.replace(/%BASE%/gi, base);
    newregexp = newregexp.replace(/%NUM%/gi, 4);
    const regexpMutationVertical = new RegExp(newregexp, 'g');
    const isMutation = testDNA(regexpMutationVertical, dna);
    if (isMutation) {
      return true;
    }
  }

  for (const base of bases) {
    let newregexp = baseRegExp.replace(/%BASE%/gi, base);
    newregexp = newregexp.replace(/%NUM%/gi, 6);
    const regexpMutationVertical = new RegExp(newregexp, 'g');
    const isMutation = testDNA(regexpMutationVertical, dna);
    if (isMutation) {
      return true;
    }
  }


  return false;
}

function testDNA(regexp, dna) {
  if (regexp.test(dna)) {
    return true;
  }

  return false;
}

module.exports = hasMutation;
