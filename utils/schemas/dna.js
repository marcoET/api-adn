const joi = require('joi')

const dnaSchema = {
    dna: joi.array().length(6).items(joi.string().pattern(new RegExp('[ATCG]{6}$')))
}

module.exports = {
    dnaSchema
}