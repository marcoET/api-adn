const express = require('express');
const responses = require('../utils/network/response');
const DNAService = require('../services/dna');

const validationHandler = require('../utils/middleware/validationHandler');
const { dnaSchema } = require('../utils/schemas/dna')

function dnaApi(app) {
  const router = express.Router();
  const dnaService = new DNAService();

  app.use('/dna', router);

  router.post('/mutation', validationHandler(dnaSchema),async function (req, res, next) {
    const { body: dna } = req;
    try {
      const isMutation = await dnaService.hasMutatio(dna);
      if(isMutation){
        return responses.success(req, res, 'valid dna', 200);
      }
      responses.success(req, res, 'mutation dna', 403);
    } catch (err) {
      next(err);
    }
  });
}

module.exports = dnaApi;
