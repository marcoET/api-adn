const validDNA = {
  dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
};
const mutationDNA = {
  dna: ['ATGCGA', 'CTGTGC', 'TTATTT', 'ATACGG', 'GCGTCA', 'TCACTG'],
};

const testServer = require('../../utils/testServer');
const route = require('../../routes/dna');

describe('routes - DNA', () => {
  const request = testServer(route);
  describe('POST /dna/mutation', () => {
    test('should respond with status 200', async (done) => {
      const res = await request.post('/dna/mutation').send(validDNA);
      expect(res.status).toEqual(200);
      done();
    });

    test('should respond with status 403', async (done) => {
      const res = await request.post('/dna/mutation').send(mutationDNA);
      expect(res.status).toEqual(403);
      done();
    });
  });
});
