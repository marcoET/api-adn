const mutation = require('../../utils/mutation');

describe('utils - mutation', () => {
  test('multiple mutations', () => {
    expect(mutation(['ATGCGA','CCCCAA','TTCTGA','AAGCTA','GCGACA','TTTTGG'])).toBe(false);
  });
  test('invalid dna', () => {
    expect(mutation(['ATGCGA','CCCCAA','TTCTGA','AAHCTA','GCGACA','TTTTGG'])).toBe(false);
  });
  test('not mutations', () => {
    expect(mutation(['ATGCGA','CAGTGC','TTATTT','AGACGG','GCGTCA','TCACTG'])).toBe(true);
  });
  test('horizontal mutations', () => {
    expect(mutation(['ATGCGA','CAGTGC','TTTTTT','AGACGG','GCGTCA','TCACTG'])).toBe(false);
  });
  test('vertical mutations', () => {
    expect(mutation(['ATGCGA','CTGTGC','TTATTT','ATACGG','GCGTCA','TCACTG'])).toBe(false);
  });
  test('diagonal mutations 1', () => {
    expect(mutation(['ATGCGA','CAGTAC','TATATT','AGACGG','GCGTCA','TCACTG'])).toBe(false);
  });
  test('diagonal mutations 2', () => {
    expect(mutation(['ATGCGA','CAGGGC','TTATGT','ACACTG','GCGTCA','TCACTG'])).toBe(false);
  });
});
